# LegacyIP Warn

Fork from: [https://gitlab.com/margau_me/legacyip-warn](https://gitlab.com/margau_me/legacyip-warn)


This project allows you to inject an warning banner for IPv4-users with a NGINX webserver.

It consists of two snippets and an example vhost configuration.

Blog Post: [https://margau.net/posts/2020-09-06-legacyip-warn/](https://margau.net/posts/2020-09-06-legacyip-warn/)


# Install

`mkdir /etc/nginx/snippets`

copy `legacy-banner.conf` to  `/etc/nginx/snippets/legacy-banner.conf`

copy `legacy-js.conf` to `/etc/nginx/snippets/legacy-js.conf`

Add the script to the site config like this:

                geo $ipv4 {
                    0.0.0.0/0 ipv4;
                }
                server {
	                #Listen SSL+HTTP/2
	                listen 443 ssl http2;
	                listen [::]:443 ssl http2;

	                #Server Name here

	                include /etc/nginx/snippets/legacy-js.conf;

	                location / {
		
		            #normal location or proxy settings

	            	# IP Legacy Banner
		            include /etc/nginx/snippets/legacy-banner.conf;
		            if ($ipv4 ~ "^$") {
	                    set $legacy "<!-- uses IPv6 --></body>";
		            }
		            sub_filter "script-src 'unsafe-inline'" "script-src 'unsafe-inline'";
		            sub_filter "</body>" $legacy;
	            	}
				}
